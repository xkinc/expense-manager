﻿using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Data
{
    public class ManagerDbContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        public virtual DbSet<User> IdentityUsers { get; set; }

        public virtual DbSet<Transaction> Transactions { get; set; }

        public virtual DbSet<Vendor> Vendors { get; set; }

        public virtual DbSet<Category> Categories { get; set; }

        public virtual DbSet<Plan> Plans { get; set; }
        public virtual DbSet<TransactionCategory> TransactionCategories { get; set; }

        public ManagerDbContext(DbContextOptions<ManagerDbContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder
                .Entity<TransactionCategory>()
                .HasKey(tc => new { tc.TransactionId, tc.CategoryId });

            builder
                .Entity<Transaction>()
                .HasMany(transaction => transaction.Categories)
                .WithMany()
                .UsingEntity<TransactionCategory>(j => j.ToTable("TransactionCategories"));

            foreach (
                var relationship in builder
                    .Model.GetEntityTypes()
                    .SelectMany(e => e.GetForeignKeys())
            )
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            builder.Seed();

            base.OnModelCreating(builder);
        }
    }
}
