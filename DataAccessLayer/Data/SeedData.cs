﻿using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Data
{
    public static class SeedData
    {
        private static readonly int _userId = 1;
        private static readonly int _adminId = 2;

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var roles = RoleSeed();
            var users = UserSeed();
            var userRoles = UserRolesSeed();
            var vendors = VendorSeed();
            var transactions = TransactionSeed();
            var categories = CategorySeed();
            var plans = PlanSeed();
            var transactionCategories = TransactionCategorySeed();

            modelBuilder.Entity<IdentityRole<int>>().HasData(roles);

            modelBuilder.Entity<User>().HasData(users);

            modelBuilder.Entity<IdentityUserRole<int>>().HasData(userRoles);

            modelBuilder.Entity<Transaction>().HasData(transactions);

            modelBuilder.Entity<Category>().HasData(categories);

            modelBuilder.Entity<Vendor>().HasData(vendors);

            modelBuilder.Entity<Plan>().HasData(plans);

            modelBuilder.Entity<TransactionCategory>().HasData(transactionCategories);
        }

        private static List<IdentityRole<int>> RoleSeed()
        {
            List<IdentityRole<int>> roles =
            [
                new IdentityRole<int>("User") { Id = _userId, },
                new IdentityRole<int>("Admin") { Id = _adminId, }
            ];
            return roles;
        }

        private static List<User> UserSeed()
        {
            List<User> users = new();
            var hasher = new PasswordHasher<User>();
            string[] emails =
            [
                "admin@example.com",
                "user1@example.com",
                "user2@example.com",
                "user3@example.com"
            ];
            var userData = new List<(string UserName, string Email, string Password, int Id)>
            {
                // passwords visible for simplicity (and running the app without additional configuration)
                (emails[0], emails[0], "Ahoj123,", 1),
                (emails[1], emails[1], "Ahoj123,", 2),
                (emails[2], emails[2], "AFSDd-FSDFBCVEWR87", 3),
                (emails[3], emails[3], "dsfsdfDSFO-s785sd", 4),
            };
            foreach (var (name, email, pass, id) in userData)
            {
                var user = new User
                {
                    UserName = name,
                    NormalizedUserName = name.ToUpper(),
                    Email = email,
                    NormalizedEmail = email.ToUpper(),
                    Id = id
                };
                user.PasswordHash = hasher.HashPassword(user, pass);
                user.SecurityStamp = Guid.NewGuid().ToString();
                users.Add(user);
            }
            return users;
        }

        private static List<IdentityUserRole<int>> UserRolesSeed()
        {
            List<IdentityUserRole<int>> userRoles = [];
            userRoles.Add(new IdentityUserRole<int>() { UserId = 1, RoleId = _adminId, });
            userRoles.Add(new IdentityUserRole<int>() { UserId = 2, RoleId = _userId, });
            userRoles.Add(new IdentityUserRole<int>() { UserId = 3, RoleId = _userId, });
            userRoles.Add(new IdentityUserRole<int>() { UserId = 4, RoleId = _userId, });
            return userRoles;
        }

        private static List<Plan> PlanSeed()
        {
            return
            [
                new Plan
                {
                    Id = 1,
                    Goal = 200M,
                    Name = "My Plan",
                    Description = "My budget for April",
                    From = new DateTime(2024, 4, 1),
                    To = new DateTime(2024, 4, 30),
                    UserId = 1
                },
            ];
        }

        private static List<Vendor> VendorSeed()
        {
            return
            [
                new Vendor
                {
                    Id = 1,
                    Name = "Shopping centre",
                    Description = "Shopping centre with various attractions."
                },
                new Vendor
                {
                    Id = 2,
                    Name = "Job",
                    Description = ""
                },
            ];
        }

        private static List<Transaction> TransactionSeed()
        {
            return
            [
                new Transaction
                {
                    Id = 1,
                    CreatedAt = new DateTime(2024, 4, 5),
                    Amount = 100.54M,
                    TransactionType = TransactionType.Expense,
                    TransactionFrequency = TransactionFrequency.OneTime,
                    Description = "",
                    UserId = 1,
                    VendorId = 1,
                },
                new Transaction
                {
                    Id = 2,
                    CreatedAt = new DateTime(2024, 4, 30),
                    Amount = 200.54M,
                    TransactionType = TransactionType.Income,
                    TransactionFrequency = TransactionFrequency.OneTime,
                    Description = "",
                    UserId = 1,
                    VendorId = 2,
                },
                new Transaction
                {
                    Id = 3,
                    Amount = 100.54M,
                    TransactionType = TransactionType.Expense,
                    TransactionFrequency = TransactionFrequency.OneTime,
                    Description = "",
                    UserId = 1,
                    VendorId = 1,
                },
                new Transaction
                {
                    Id = 4,
                    Amount = 500.54M,
                    TransactionType = TransactionType.Expense,
                    TransactionFrequency = TransactionFrequency.OneTime,
                    Description = "Cinema",
                    UserId = 2,
                    VendorId = 1,
                },
            ];
        }

        private static List<Category> CategorySeed()
        {
            return
            [
                new Category { Id = 1, Name = "Entertainment", },
                new Category { Id = 2, Name = "Employment", },
                new Category { Id = 3, Name = "Basic needs", },
            ];
        }

        private static List<TransactionCategory> TransactionCategorySeed()
        {
            return
            [
                new TransactionCategory { TransactionId = 2, CategoryId = 2 },
                new TransactionCategory { TransactionId = 1, CategoryId = 1, },
                new TransactionCategory { TransactionId = 3, CategoryId = 1 },
                new TransactionCategory { TransactionId = 3, CategoryId = 3 },
                new TransactionCategory { TransactionId = 4, CategoryId = 1 },
            ];
        }
    }
}
