﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace DataAccessLayer.Migrations
{
    /// <inheritdoc />
    public partial class InitialMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table
                        .Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(
                        type: "TEXT",
                        maxLength: 256,
                        nullable: true
                    ),
                    ConcurrencyStamp = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                }
            );

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table
                        .Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(
                        type: "TEXT",
                        maxLength: 256,
                        nullable: true
                    ),
                    Email = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(
                        type: "TEXT",
                        maxLength: 256,
                        nullable: true
                    ),
                    EmailConfirmed = table.Column<bool>(type: "INTEGER", nullable: false),
                    PasswordHash = table.Column<string>(type: "TEXT", nullable: true),
                    SecurityStamp = table.Column<string>(type: "TEXT", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "TEXT", nullable: true),
                    PhoneNumber = table.Column<string>(type: "TEXT", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "INTEGER", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "INTEGER", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "TEXT", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "INTEGER", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                }
            );

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table
                        .Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                }
            );

            migrationBuilder.CreateTable(
                name: "Plans",
                columns: table => new
                {
                    Id = table
                        .Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    Goal = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Description = table.Column<string>(
                        type: "TEXT",
                        maxLength: 250,
                        nullable: false
                    ),
                    From = table.Column<DateTime>(type: "TEXT", nullable: false),
                    To = table.Column<DateTime>(type: "TEXT", nullable: false),
                    UserId = table.Column<int>(type: "INTEGER", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plans", x => x.Id);
                }
            );

            migrationBuilder.CreateTable(
                name: "Vendors",
                columns: table => new
                {
                    Id = table
                        .Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", maxLength: 60, nullable: false),
                    Description = table.Column<string>(
                        type: "TEXT",
                        maxLength: 250,
                        nullable: false
                    ),
                    CreatedAt = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendors", x => x.Id);
                }
            );

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table
                        .Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RoleId = table.Column<int>(type: "INTEGER", nullable: false),
                    ClaimType = table.Column<string>(type: "TEXT", nullable: true),
                    ClaimValue = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade
                    );
                }
            );

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table
                        .Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<int>(type: "INTEGER", nullable: false),
                    ClaimType = table.Column<string>(type: "TEXT", nullable: true),
                    ClaimValue = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade
                    );
                }
            );

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "TEXT", nullable: false),
                    ProviderKey = table.Column<string>(type: "TEXT", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "TEXT", nullable: true),
                    UserId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey(
                        "PK_AspNetUserLogins",
                        x => new { x.LoginProvider, x.ProviderKey }
                    );
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade
                    );
                }
            );

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "INTEGER", nullable: false),
                    RoleId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade
                    );
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade
                    );
                }
            );

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "INTEGER", nullable: false),
                    LoginProvider = table.Column<string>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Value = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey(
                        "PK_AspNetUserTokens",
                        x => new
                        {
                            x.UserId,
                            x.LoginProvider,
                            x.Name
                        }
                    );
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade
                    );
                }
            );

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Id = table
                        .Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Description = table.Column<string>(
                        type: "TEXT",
                        maxLength: 250,
                        nullable: false
                    ),
                    TransactionType = table.Column<int>(type: "INTEGER", nullable: false),
                    TransactionFrequency = table.Column<int>(type: "INTEGER", nullable: false),
                    UserId = table.Column<int>(type: "INTEGER", nullable: false),
                    VendorId = table.Column<int>(type: "INTEGER", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transactions_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict
                    );
                    table.ForeignKey(
                        name: "FK_Transactions_Vendors_VendorId",
                        column: x => x.VendorId,
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict
                    );
                }
            );

            migrationBuilder.CreateTable(
                name: "TransactionCategories",
                columns: table => new
                {
                    TransactionId = table.Column<int>(type: "INTEGER", nullable: false),
                    CategoryId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey(
                        "PK_TransactionCategories",
                        x => new { x.TransactionId, x.CategoryId }
                    );
                    table.ForeignKey(
                        name: "FK_TransactionCategories_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict
                    );
                    table.ForeignKey(
                        name: "FK_TransactionCategories_Transactions_TransactionId",
                        column: x => x.TransactionId,
                        principalTable: "Transactions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict
                    );
                }
            );

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, null, "User", null },
                    { 2, null, "Admin", null }
                }
            );

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[]
                {
                    "Id",
                    "AccessFailedCount",
                    "ConcurrencyStamp",
                    "Email",
                    "EmailConfirmed",
                    "LockoutEnabled",
                    "LockoutEnd",
                    "NormalizedEmail",
                    "NormalizedUserName",
                    "PasswordHash",
                    "PhoneNumber",
                    "PhoneNumberConfirmed",
                    "SecurityStamp",
                    "TwoFactorEnabled",
                    "UserName"
                },
                values: new object[,]
                {
                    {
                        1,
                        0,
                        "64710fe1-de30-4797-bc76-6ba62bc8bdd9",
                        "admin@example.com",
                        false,
                        false,
                        null,
                        "ADMIN@EXAMPLE.COM",
                        "ADMIN@EXAMPLE.COM",
                        "AQAAAAIAAYagAAAAELYScoSkStY+4VvEaNu8DvjmlNJjKywsV67MF4aEeRHGHmdfwEtvzQPCR9R3+6Tjkw==",
                        null,
                        false,
                        "f7dcd954-3aa3-4cbe-ba5d-7514991c76c8",
                        false,
                        "admin@example.com"
                    },
                    {
                        2,
                        0,
                        "a1a712f2-4884-45c7-b1ee-034254102428",
                        "user1@example.com",
                        false,
                        false,
                        null,
                        "USER1@EXAMPLE.COM",
                        "USER1@EXAMPLE.COM",
                        "AQAAAAIAAYagAAAAEFbHMndD1FBQgKy6DBbhW0EUHfh9vF61ZqCcBdN001Wp3+vfKKyLHA1YWlZ20Uej1g==",
                        null,
                        false,
                        "06fb5b7b-e84b-4015-8a35-1977055d4468",
                        false,
                        "user1@example.com"
                    },
                    {
                        3,
                        0,
                        "ad9b8dcd-1e52-4b1b-954d-e588650008b6",
                        "user2@example.com",
                        false,
                        false,
                        null,
                        "USER2@EXAMPLE.COM",
                        "USER2@EXAMPLE.COM",
                        "AQAAAAIAAYagAAAAEEwuNA0dYf9fHOK+OFyehzRBttI5t7opXK4C6FwN2CULYb5BJvoo9K1w5ZaeuSM9vw==",
                        null,
                        false,
                        "461ad7a9-2fe4-42a2-9fed-fa3a4fca4959",
                        false,
                        "user2@example.com"
                    },
                    {
                        4,
                        0,
                        "ad81416a-be17-4042-b03a-84d5f0a0a266",
                        "user3@example.com",
                        false,
                        false,
                        null,
                        "USER3@EXAMPLE.COM",
                        "USER3@EXAMPLE.COM",
                        "AQAAAAIAAYagAAAAEE9zJJIlx0nxR9RDPaIsQViM6lDTtBX1dIZ70cndGTA3xwLL6DgZnToG69NvMDY3Mw==",
                        null,
                        false,
                        "b3fec644-2ffe-4b2a-bdb8-214da6234743",
                        false,
                        "user3@example.com"
                    }
                }
            );

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    {
                        1,
                        new DateTime(2024, 6, 5, 19, 33, 53, 452, DateTimeKind.Local).AddTicks(
                            4757
                        ),
                        "Entertainment"
                    },
                    {
                        2,
                        new DateTime(2024, 6, 5, 19, 33, 53, 452, DateTimeKind.Local).AddTicks(
                            4760
                        ),
                        "Employment"
                    },
                    {
                        3,
                        new DateTime(2024, 6, 5, 19, 33, 53, 452, DateTimeKind.Local).AddTicks(
                            4762
                        ),
                        "Basic needs"
                    }
                }
            );

            migrationBuilder.InsertData(
                table: "Plans",
                columns: new[]
                {
                    "Id",
                    "CreatedAt",
                    "Description",
                    "From",
                    "Goal",
                    "Name",
                    "To",
                    "UserId"
                },
                values: new object[]
                {
                    1,
                    new DateTime(2024, 6, 5, 19, 33, 53, 452, DateTimeKind.Local).AddTicks(4838),
                    "My budget for April",
                    new DateTime(2024, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                    200m,
                    "My Plan",
                    new DateTime(2024, 4, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                    1
                }
            );

            migrationBuilder.InsertData(
                table: "Vendors",
                columns: new[] { "Id", "CreatedAt", "Description", "Name" },
                values: new object[,]
                {
                    {
                        1,
                        new DateTime(2024, 6, 5, 19, 33, 53, 452, DateTimeKind.Local).AddTicks(
                            4640
                        ),
                        "Shopping centre with various attractions.",
                        "Shopping centre"
                    },
                    {
                        2,
                        new DateTime(2024, 6, 5, 19, 33, 53, 452, DateTimeKind.Local).AddTicks(
                            4702
                        ),
                        "",
                        "Job"
                    }
                }
            );

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { 2, 1 },
                    { 1, 2 },
                    { 1, 3 },
                    { 1, 4 }
                }
            );

            migrationBuilder.InsertData(
                table: "Transactions",
                columns: new[]
                {
                    "Id",
                    "Amount",
                    "CreatedAt",
                    "Description",
                    "TransactionFrequency",
                    "TransactionType",
                    "UserId",
                    "VendorId"
                },
                values: new object[,]
                {
                    {
                        1,
                        100.54m,
                        new DateTime(2024, 4, 5, 0, 0, 0, 0, DateTimeKind.Unspecified),
                        "",
                        0,
                        0,
                        1,
                        1
                    },
                    {
                        2,
                        200.54m,
                        new DateTime(2024, 4, 30, 0, 0, 0, 0, DateTimeKind.Unspecified),
                        "",
                        0,
                        1,
                        1,
                        2
                    },
                    {
                        3,
                        100.54m,
                        new DateTime(2024, 6, 5, 19, 33, 53, 452, DateTimeKind.Local).AddTicks(
                            4725
                        ),
                        "",
                        0,
                        0,
                        1,
                        1
                    },
                    {
                        4,
                        500.54m,
                        new DateTime(2024, 6, 5, 19, 33, 53, 452, DateTimeKind.Local).AddTicks(
                            4740
                        ),
                        "Cinema",
                        0,
                        0,
                        2,
                        1
                    }
                }
            );

            migrationBuilder.InsertData(
                table: "TransactionCategories",
                columns: new[] { "CategoryId", "TransactionId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 },
                    { 1, 3 },
                    { 3, 3 },
                    { 1, 4 }
                }
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId"
            );

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId"
            );

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail"
            );

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true
            );

            migrationBuilder.CreateIndex(
                name: "IX_Categories_Name",
                table: "Categories",
                column: "Name",
                unique: true
            );

            migrationBuilder.CreateIndex(
                name: "IX_TransactionCategories_CategoryId",
                table: "TransactionCategories",
                column: "CategoryId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_UserId",
                table: "Transactions",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_VendorId",
                table: "Transactions",
                column: "VendorId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_Name",
                table: "Vendors",
                column: "Name",
                unique: true
            );
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "AspNetRoleClaims");

            migrationBuilder.DropTable(name: "AspNetUserClaims");

            migrationBuilder.DropTable(name: "AspNetUserLogins");

            migrationBuilder.DropTable(name: "AspNetUserRoles");

            migrationBuilder.DropTable(name: "AspNetUserTokens");

            migrationBuilder.DropTable(name: "Plans");

            migrationBuilder.DropTable(name: "TransactionCategories");

            migrationBuilder.DropTable(name: "AspNetRoles");

            migrationBuilder.DropTable(name: "Categories");

            migrationBuilder.DropTable(name: "Transactions");

            migrationBuilder.DropTable(name: "AspNetUsers");

            migrationBuilder.DropTable(name: "Vendors");
        }
    }
}
