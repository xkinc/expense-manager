﻿namespace DataAccessLayer.Entities
{
    public enum TransactionFrequency
    {
        OneTime,
        ByMinute,
        Daily,
        Weekly,
        Monthly
    }
}
