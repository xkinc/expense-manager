﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{
    public class Plan : BaseEntity
    {
        [Required]
        [MaxLength(60)]
        public string Name { get; set; } = "";

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal Goal { get; set; }

        [MaxLength(250)]
        public string Description { get; set; } = "";

        [Required]
        public DateTime From { get; set; }

        [Required]
        public DateTime To { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
    }
}
