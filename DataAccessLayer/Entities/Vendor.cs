﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Entities
{
    [Index(nameof(Name), IsUnique = true)]
    public class Vendor : BaseEntity
    {
        [Required]
        [StringLength(60, MinimumLength = 2)]
        public string Name { get; set; } = "Default";

        [MaxLength(250)]
        public string Description { get; set; } = "";
    }
}
