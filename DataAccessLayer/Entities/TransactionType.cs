﻿namespace DataAccessLayer.Entities
{
    public enum TransactionType
    {
        Expense,
        Income
    }
}
