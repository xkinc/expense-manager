﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{
    public class TransactionCategory
    {
        [ForeignKey("Transaction")]
        public int TransactionId { get; set; }

        [ForeignKey("Category")]
        public int CategoryId { get; set; }
    }
}
