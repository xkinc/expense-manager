﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.Entities
{
    public class Transaction : BaseEntity
    {
        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }

        [MaxLength(250)]
        public string Description { get; set; } = "";

        [Required]
        public required TransactionType TransactionType { get; set; }

        [Required]
        public required TransactionFrequency TransactionFrequency { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }

        public virtual User? User { get; set; }

        [ForeignKey("Vendor")]
        public int VendorId { get; set; }

        public virtual Vendor? Vendor { get; set; }

        public virtual ICollection<Category> Categories { get; set; } = [];
    }
}
