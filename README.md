# Expense-Manager
Welcome to Expense manager web application! This application makes management of transactions simple and easy to understand with graphical visualisation.

## Getting started
To start the application, you need to use migrations files in DataAccessLayer/Migrations.
In Visual studio you can navigate to package manager console and execute 'Update-Database', this should seed the database with tables and initial data.
Afterwards you can start the application from IDE, or navigate to ExpenseManager project and build and run the applkication with 'dotnet build' and 'dotnet run'.
You can login as any seeded user (password is for easier use in open form in SeedData, in database passwords are hashed). Or you can register as a new user.

## Roles
Basic roles are User and Admin, Admin is same as user, but can manage categories.

## Additional comment
Plan and Vendor are not fully implemented in MVC and Admin is almost same as User (user management for admin could be added). Although the application is ready for these extensions.