﻿using BusinessLayer.Models;
using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ExpenseManager.Controllers
{
    public enum TimeRange
    {
        AllTime,
        LastYear,
        LastMonth,
        LastDay
    }

    [Authorize]
    public class GraphController : Controller
    {
        private readonly ITransactionService _transactionService;
        private readonly UserManager<User> _userManager;

        public GraphController(ITransactionService tService, UserManager<User> userManager)
        {
            _transactionService = tService;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index(
            FilterOptionsDto? options = null,
            TimeRange? timeRange = null
        )
        {
            var user = await _userManager.GetUserAsync(User);
            options ??= new FilterOptionsDto();
            options.UserId = user!.Id;

            DateTime? startDate;

            switch (timeRange)
            {
                case TimeRange.LastYear:
                    startDate = DateTime.Now.AddYears(-1);
                    break;
                case TimeRange.LastMonth:
                    startDate = DateTime.Now.AddMonths(-1);
                    break;
                case TimeRange.LastDay:
                    startDate = DateTime.Now.AddDays(-1);
                    break;
                default:
                    startDate = null;
                    break;
            }
            options.From ??= startDate;
            var transactions = await _transactionService.GetAllByOptionsAsync(options);

            ViewBag.SelectedTimeRange = timeRange;
            return View(transactions);
        }
    }
}
