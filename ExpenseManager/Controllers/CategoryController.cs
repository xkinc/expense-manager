﻿using BusinessLayer.Models;
using BusinessLayer.Services.Interfaces;
using ExpenseManager.Models;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ExpenseManager.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService cService)
        {
            _categoryService = cService;
        }

        // GET: Category
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List()
        {
            var categories = await _categoryService.GetAllAsync();
            return View(categories);
        }

        // GET: Category/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var category = await _categoryService.GetByIdAsync(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Category/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CategoryModel model)
        {
            try
            {
                var category = await _categoryService.CreateAsync(model.Adapt<CategoryDto>());
                if (category == null)
                {
                    return View("CustomError", "Create failed");
                }
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                ModelState.AddModelError("Name", "A category with this name already exists.");
                return View(model);
            }
        }

        // GET: Category/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var category = await _categoryService.GetByIdAsync(id);
            if (category == null)
            {
                return NotFound(id);
            }
            return View(category.Adapt<CategoryModel>());
        }

        // POST: Category/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, CategoryModel model)
        {
            try
            {
                var category = await _categoryService.UpdateAsync(id, model.Adapt<CategoryDto>());
                if (category == null)
                {
                    return View("CustomError", "Edit failed");
                }
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                ModelState.AddModelError("Name", "A category with this name already exists.");
                return View(model);
            }
        }

        // GET: Category/Delete/5
        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            var transaction = await _categoryService.GetByIdAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }
            return View(id);
        }

        // POST: TransactionController/DeleteConfirm/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirm(int id)
        {
            try
            {
                var res = await _categoryService.DeleteAsync(id);
                if (!res)
                {
                    return StatusCode(400);
                }
                return RedirectToAction("List");
            }
            catch
            {
                return View("CustomError", "Cannot delete because of references");
            }
        }
    }
}
