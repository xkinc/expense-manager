﻿using BusinessLayer.Models;
using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Entities;
using ExpenseManager.Models;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ExpenseManager.Controllers
{
    [Authorize]
    public class TransactionController : Controller
    {
        private readonly ITransactionService _transactionService;
        private readonly IVendorService _vendorService;
        private readonly ICategoryService _categoryService;
        private readonly UserManager<User> _userManager;

        public TransactionController(
            ITransactionService tService,
            IVendorService vService,
            ICategoryService cService,
            UserManager<User> userManager
        )
        {
            _transactionService = tService;
            _vendorService = vService;
            _categoryService = cService;
            _userManager = userManager;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        // GET: TransactionController/List
        public async Task<ActionResult> List(FilterOptionsDto options)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return Unauthorized();
            }
            options.UserId = user.Id;

            var transactions = await _transactionService.GetAllByOptionsAsync(options);

            ViewBag.Vendors = await GetVendorsOptions();
            ViewBag.Categories = await GetCategoriesOptions();
            return View(
                new FilterViewModel { FilterOptions = options, Transactions = transactions }
            );
        }

        // GET: TransactionController/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var transaction = await _transactionService.GetByIdAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }
            if (!(await UserIdMatch(transaction.UserId)))
            {
                return Unauthorized();
            }
            return View(transaction);
        }

        // GET: TransactionController/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Vendors = await GetVendorsOptions();
            ViewBag.Categories = await GetCategoriesOptions();
            return View();
        }

        // POST: TransactionController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TransactionChangeDto model)
        {
            try
            {
                model.Description ??= "";
                if (ModelState.IsValid)
                {
                    var transactionDto = model;

                    var user = await _userManager.GetUserAsync(User);
                    if (user == null)
                    {
                        return Unauthorized();
                    }
                    transactionDto.UserId = user.Id;
                    var res = await _transactionService.CreateAsync(transactionDto);
                    if (res == null)
                    {
                        return StatusCode(500);
                    }
                    return RedirectToAction("List");
                }
                ViewBag.Vendors = await GetVendorsOptions();
                ViewBag.Categories = await GetCategoriesOptions();
                return View(model);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        // GET: TransactionController/Edit/5
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var transaction = await _transactionService.GetByIdAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }
            var viewDto = (TransactionViewDto)transaction;
            ViewBag.Vendors = await GetVendorsOptions();
            ViewBag.Categories = await GetCategoriesOptions();
            var model = viewDto.Adapt<TransactionChangeDto>();
            model.CategoriesIds = viewDto.Categories.Select(c => c.Id).ToList();
            model.VendorId = viewDto.Vendor?.Id ?? 0;
            return View(model);
        }

        // POST: TransactionController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, TransactionChangeDto model)
        {
            model.Description ??= "";
            if (!ModelState.IsValid)
            {
                ViewBag.Vendors = await GetVendorsOptions();
                ViewBag.Categories = await GetCategoriesOptions();
                return View(model);
            }

            await _transactionService.UpdateAsync(id, model);

            return RedirectToAction("Details", new { id });
        }

        // GET: TransactionController/Delete/5
        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            var transaction = await _transactionService.GetByIdAsync(id);
            if (transaction == null)
            {
                return NotFound();
            }
            if (!await UserIdMatch(((TransactionViewDto)transaction).UserId))
            {
                return Unauthorized();
            }
            return View(id);
        }

        // POST: TransactionController/DeleteConfirm/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirm(int id)
        {
            try
            {
                var res = await _transactionService.DeleteAsync(id);
                if (!res)
                {
                    return StatusCode(400);
                }
                return RedirectToAction("List");
            }
            catch
            {
                return View("CustomError", "Cannot delete because of references");
            }
        }

        private async Task<bool> UserIdMatch(int userId)
        {
            var user = await _userManager.GetUserAsync(User);

            if (user == null)
            {
                return false;
            }
            return user.Id == userId;
        }

        private async Task<IEnumerable<SelectListItem>?> GetVendorsOptions()
        {
            return (await _vendorService.GetAllAsync()).Select(v => new SelectListItem
            {
                Value = v.Id.ToString(),
                Text = v.Name
            });
        }

        private async Task<IEnumerable<SelectListItem>?> GetCategoriesOptions()
        {
            return (await _categoryService.GetAllAsync()).Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Name
            });
        }
    }
}
