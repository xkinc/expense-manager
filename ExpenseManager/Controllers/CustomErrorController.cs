﻿using Microsoft.AspNetCore.Mvc;

namespace ExpenseManager.Controllers
{
    [Route("Error")]
    public class CustomErrorController : Controller
    {
        public IActionResult Index()
        {
            return View("CustomError", "An error occurred. Please try again later.");
        }
    }
}
