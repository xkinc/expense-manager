﻿using BusinessLayer.Models;
using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Entities;
using ExpenseManager.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ExpenseManager.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private readonly ITransactionService _transactionService;
        private readonly UserManager<User> _userManager;

        public DashboardController(ITransactionService tService, UserManager<User> userManager)
        {
            _transactionService = tService;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return Unauthorized();
            }
            var options = new FilterOptionsDto { UserId = user.Id, };
            var transactions = await _transactionService.GetAllByOptionsAsync(options);
            var model = CreateDashboardModel(transactions);

            return View(model);
        }

        private DashboardViewModel CreateDashboardModel(List<TransactionViewDto> transactions)
        {
            var income = transactions
                .Where(t => t.TransactionType == TransactionType.Income)
                .Sum(t => t.Amount);
            var expense = transactions
                .Where(t => t.TransactionType == TransactionType.Expense)
                .Sum(t => t.Amount);
            var vendorName = transactions
                .Select(t => t.Vendor?.Name)
                .GroupBy(name => name)
                .OrderByDescending(g => g.Count())
                .Select(g => g.Key)
                .FirstOrDefault();
            var categoryName = transactions
                .SelectMany(t => t.Categories)
                .GroupBy(c => c.Name)
                .OrderByDescending(g => g.Count())
                .Select(g => g.Key)
                .FirstOrDefault();
            var model = new DashboardViewModel
            {
                TotalIncome = income,
                TotalExpense = expense,
                Balance = income - expense,
                NumberOfTransactions = transactions.Count,
                VendorName = vendorName ?? "None",
                CategoryName = categoryName ?? "None"
            };
            return model;
        }
    }
}
