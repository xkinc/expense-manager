﻿using System.Text;
using System.Text.Json;
using BusinessLayer.Models;
using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ExpenseManager.Controllers
{
    [Authorize]
    public class TransferController : Controller
    {
        private readonly ITransactionService _transactionService;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<TransferController> _logger;

        public TransferController(
            ITransactionService tService,
            UserManager<User> userManager,
            ILogger<TransferController> logger
        )
        {
            _transactionService = tService;
            _userManager = userManager;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Export(FilterOptionsDto? options)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                options ??= new FilterOptionsDto();
                options.UserId = user!.Id;
                var transactions = await _transactionService.GetAllByOptionsAsync(options);

                var json = JsonSerializer.Serialize(transactions);

                var fileName = "transactions.json";
                return File(Encoding.UTF8.GetBytes(json), "application/json", fileName);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return View("CustomError", "File export failed.");
            }
        }

        [HttpGet]
        public IActionResult Import()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Import(IFormFile file)
        {
            try
            {
                if (file != null && file.Length > 0)
                {
                    var user = await _userManager.GetUserAsync(User);
                    if (user == null)
                    {
                        return View("CustomError", "User not logged in");
                    }
                    var (success, message) = await _transactionService.ImportTransactions(
                        file,
                        user
                    );
                    if (!success)
                    {
                        return View("CustomError", message);
                    }
                    return RedirectToAction("List", "Transaction");
                }
                return View("CustomError", "File upload failed.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return View("CustomError", "File upload failed.");
            }
        }
    }
}
