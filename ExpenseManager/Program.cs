using BusinessLayer;
using BusinessLayer.Services.Implementations;
using ExpenseManager;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllersWithViews(options => { });

var dbName = "expense_manager.db";

var folder = Environment.SpecialFolder.LocalApplicationData;
var dbPath = Path.Join(Environment.GetFolderPath(folder), dbName);
var connectionString = $"Data Source={dbPath}";
builder.Services.AddServices(connectionString);
builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = "/Account/Login";
});
builder.Services.AddHostedService<TimerService>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");

app.UseGlobalExceptionHandler();

app.Run();
