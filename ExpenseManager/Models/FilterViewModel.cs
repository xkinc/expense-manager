﻿using BusinessLayer.Models;

namespace ExpenseManager.Models
{
    public class FilterViewModel
    {
        public required FilterOptionsDto FilterOptions { get; set; }
        public required IEnumerable<TransactionViewDto> Transactions { get; set; }
    }
}
