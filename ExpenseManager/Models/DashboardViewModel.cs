﻿namespace ExpenseManager.Models
{
    public class DashboardViewModel
    {
        public decimal Balance { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalExpense { get; set; }
        public decimal NumberOfTransactions { get; set; }
        public required string VendorName { get; set; }
        public required string CategoryName { get; set; }
    }
}
