using System.ComponentModel.DataAnnotations;

namespace ExpenseManager.Models
{
    public class CategoryModel
    {
        [Required(ErrorMessage = "The Name field is required.")]
        [StringLength(
            50,
            MinimumLength = 3,
            ErrorMessage = "The Name must be between 3 and 50 characters."
        )]
        public required string Name { get; set; }
    }
}
