﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BusinessLayer.Models
{
    public class VendorDto
    {
        [JsonIgnore]
        public int Id { get; set; }

        [Required]
        [StringLength(60, MinimumLength = 2)]
        public string Name { get; set; } = "Default";

        [MaxLength(250)]
        public string Description { get; set; } = "";
    }
}
