﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BusinessLayer.Models
{
    public class PlanDto
    {
        public int? Id { get; set; }

        [Required]
        [MaxLength(60)]
        public string Name { get; set; } = "";

        [Required]
        [Column(TypeName = "decimal(18,2)")]
        public decimal Goal { get; set; }

        [MaxLength(250)]
        public string Description { get; set; } = "";

        [Required]
        public DateTime From { get; set; }

        [Required]
        public DateTime To { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
    }
}
