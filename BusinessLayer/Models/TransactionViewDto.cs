﻿using System.Text.Json.Serialization;

namespace BusinessLayer.Models
{
    public class TransactionViewDto : TransactionDto
    {
        [JsonIgnore]
        public int? Id { get; set; }

        [JsonIgnore]
        public UserDto? User { get; set; }

        public VendorDto? Vendor { get; set; }

        public DateTime? CreatedAt { get; set; }

        public IEnumerable<CategoryDto> Categories { get; set; } = [];
    }
}
