﻿namespace BusinessLayer.Models
{
    public class FilterOptionsDto
    {
        public int? UserId { get; set; }

        public int? LowerLimit { get; set; }

        public int? UpperLimit { get; set; }

        public bool Expense { get; set; } = true;

        public bool Income { get; set; } = true;

        public DateTime? From { get; set; }

        public DateTime? To { get; set; }

        public List<int>? VendorIds { get; set; }

        public List<int>? CategoryIds { get; set; }
    }
}
