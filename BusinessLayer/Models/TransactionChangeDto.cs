﻿using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.Models
{
    public class TransactionChangeDto : TransactionDto
    {
        public int VendorId { get; set; }

        [Required(ErrorMessage = "Choose atleast one category")]
        public required List<int> CategoriesIds { get; set; }
    }
}
