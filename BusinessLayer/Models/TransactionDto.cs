﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using DataAccessLayer.Entities;

namespace BusinessLayer.Models
{
    public class TransactionDto
    {
        [Required]
        [Column(TypeName = "decimal(18,2)")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Amount must be greater than 0.")]
        public required decimal Amount { get; set; }

        [MaxLength(250)]
        public string? Description { get; set; } = "";
        public required TransactionType TransactionType { get; set; }
        public required TransactionFrequency TransactionFrequency { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }
    }
}
