﻿using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.Models
{
    public class UserDto
    {
        [StringLength(120, MinimumLength = 2)]
        public string? Email { get; set; }
    }
}
