﻿using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.Models
{
    public class UserRegistrationDto
    {
        [Required]
        [EmailAddress]
        public required string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public required string Password { get; set; }
    }
}
