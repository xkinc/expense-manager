﻿using BusinessLayer.Models;

namespace BusinessLayer.Services.Interfaces
{
    public interface IVendorService : IBaseService<VendorDto> { }
}
