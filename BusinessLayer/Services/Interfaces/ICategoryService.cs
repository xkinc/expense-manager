﻿using BusinessLayer.Models;

namespace BusinessLayer.Services.Interfaces
{
    public interface ICategoryService : IBaseService<CategoryDto> { }
}
