﻿using BusinessLayer.Models;

namespace BusinessLayer.Services.Interfaces
{
    public interface IPlanService : IBaseService<PlanDto> { }
}
