﻿using BusinessLayer.Models;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Http;

namespace BusinessLayer.Services.Interfaces
{
    public interface ITransactionService : IBaseService<TransactionDto>
    {
        public Task CopyTransactions(TransactionFrequency freq);
        public Task<List<TransactionViewDto>> GetAllByOptionsAsync(FilterOptionsDto options);
        public Task<(bool, string)> ImportTransactions(IFormFile file, User user);
    }
}
