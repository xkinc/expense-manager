﻿namespace BusinessLayer.Services.Interfaces
{
    public interface IBaseService<TDto>
        where TDto : class
    {
        public Task<List<TDto>> GetAllAsync();
        public Task<TDto?> GetByIdAsync(int id);
        public Task<TDto?> CreateAsync(TDto dto);
        public Task<TDto?> UpdateAsync(int id, TDto dto);
        public Task<bool> DeleteAsync(int id);
    }
}
