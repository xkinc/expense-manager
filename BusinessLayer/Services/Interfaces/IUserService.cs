﻿using BusinessLayer.Models;
using Microsoft.AspNetCore.Identity;

namespace BusinessLayer.Services.Interfaces
{
    public interface IUserService
    {
        Task<IdentityResult> RegisterAsync(UserRegistrationDto model);

        Task<SignInResult> LogInAsync(UserLoginDto model);

        Task LogOutAsync();
    }
}
