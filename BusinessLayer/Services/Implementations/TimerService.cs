﻿using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BusinessLayer.Services.Implementations;

public class TimerService : IHostedService, IDisposable
{
    private Timer? _minuteTimer;
    private Timer? _dailyTimer;
    private Timer? _weeklyTimer;

    private readonly IServiceProvider _serviceProvider;

    public TimerService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        var now = DateTime.Now;
        SetTimer(
            TransactionFrequency.ByMinute,
            TimeSpan.FromMinutes(1),
            new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0).AddMinutes(1) - now
        );
        SetTimer(
            TransactionFrequency.Daily,
            TimeSpan.FromDays(1),
            new DateTime(now.Year, now.Month, now.Day).AddDays(1) - now
        );
        SetTimer(
            TransactionFrequency.Weekly,
            TimeSpan.FromDays(7),
            new DateTime(now.Year, now.Month, now.Day).AddDays(7) - now
        );

        return Task.CompletedTask;
    }

    private void SetTimer(TransactionFrequency frequency, TimeSpan period, TimeSpan timeUntilTarget)
    {
        var timer = new Timer(
            callback: async _ => await AddByFreqTransaction(frequency),
            state: null,
            dueTime: timeUntilTarget >= TimeSpan.FromSeconds(0)
                ? timeUntilTarget
                : TimeSpan.FromSeconds(0),
            period: period
        );

        switch (frequency)
        {
            case TransactionFrequency.ByMinute:
                _minuteTimer = timer;
                break;
            case TransactionFrequency.Daily:
                _dailyTimer = timer;
                break;
            case TransactionFrequency.Weekly:
                _weeklyTimer = timer;
                break;
            default:
                throw new ArgumentException("Timer frequency unknown");
        }
    }

    private async Task AddByFreqTransaction(TransactionFrequency freq)
    {
        using (var scope = _serviceProvider.CreateScope())
        {
            var transactionService =
                scope.ServiceProvider.GetRequiredService<ITransactionService>();
            await transactionService.CopyTransactions(freq);

            // easiest way to do months (because of variable length)
            if (DateTime.Now.Day == 1 && freq == TransactionFrequency.Daily)
            {
                await transactionService.CopyTransactions(TransactionFrequency.Monthly);
            }
        }
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _minuteTimer?.Change(Timeout.Infinite, 0);
        _dailyTimer?.Change(Timeout.Infinite, 0);
        _weeklyTimer?.Change(Timeout.Infinite, 0);
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _minuteTimer?.Dispose();
        _dailyTimer?.Dispose();
        _weeklyTimer?.Dispose();
    }
}
