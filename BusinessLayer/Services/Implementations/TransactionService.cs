﻿using System.Text.Json;
using BusinessLayer.Models;
using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Data;
using DataAccessLayer.Entities;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer.Services.Implementations
{
    public class TransactionService : BaseService<Transaction, TransactionDto>, ITransactionService
    {
        public TransactionService(ManagerDbContext dbContext)
            : base(dbContext) { }

        public override async Task<TransactionDto?> CreateAsync(TransactionDto transactionDto)
        {
            var dto = (TransactionChangeDto)transactionDto;
            var entity = dto.Adapt<Transaction>();
            if (entity.CreatedAt == default)
            {
                entity.CreatedAt = DateTime.Now;
            }
            var categories =
                dto.CategoriesIds != null
                    ? await _dbContext
                        .Categories.Where(c => dto.CategoriesIds.Contains(c.Id))
                        .ToListAsync()
                    : new List<Category>();

            entity.Categories = categories;
            entity.UserId = dto.UserId;

            var res = _dbContext.Set<Transaction>().Add(entity);

            await _dbContext.SaveChangesAsync();

            return res.Adapt<TransactionDto?>();
        }

        public override async Task<TransactionDto?> UpdateAsync(
            int id,
            TransactionDto transactionDto
        )
        {
            var dto = (TransactionChangeDto)transactionDto;
            var entity = await GetEntityAsync(id);
            if (entity == null)
            {
                return null;
            }
            var userId = entity.UserId;
            dto.Adapt(entity);
            dto.UserId = userId;
            if (entity.CreatedAt == default)
            {
                entity.CreatedAt = DateTime.Now;
            }
            var categories =
                dto.CategoriesIds != null
                    ? await _dbContext
                        .Categories.Where(c => dto.CategoriesIds.Contains(c.Id))
                        .ToListAsync()
                    : new List<Category>();

            entity.Categories.Clear();
            entity.Categories = categories;
            entity.UserId = dto.UserId;

            var res = _dbContext.Set<Transaction>().Update(entity);

            await _dbContext.SaveChangesAsync();

            return res.Adapt<TransactionDto?>();
        }

        public async Task<List<TransactionViewDto>> GetAllByOptionsAsync(FilterOptionsDto options)
        {
            var query = _dbContext.Set<Transaction>().Include("Categories").Include("Vendor");
            ApplyFilters(ref query, options);
            var entities = await query.ToListAsync();
            return entities.Adapt<List<TransactionViewDto>>();
        }

        public override async Task<TransactionDto?> GetByIdAsync(int id)
        {
            var entity = await GetEntityAsync(id);
            if (entity == null)
            {
                return null;
            }
            return entity.Adapt<TransactionViewDto>();
        }

        public override async Task<bool> DeleteAsync(int id)
        {
            var entity = await GetEntityAsync(id);
            if (entity == null)
            {
                return false;
            }
            entity.Categories.Clear();
            var res = _dbContext.Set<Transaction>().Remove(entity);
            await _dbContext.SaveChangesAsync();
            return res != null;
        }

        public async Task CopyTransactions(TransactionFrequency freq)
        {
            var now = DateTime.Now;
            var transactions = await _dbContext
                .Transactions.Where(t => t.TransactionFrequency == freq)
                .Include(t => t.Categories)
                .ToListAsync();
            var copiedTransactions = transactions
                .Select(t =>
                {
                    var copiedTransaction = t.Adapt<Transaction>();

                    copiedTransaction.Id = 0;
                    copiedTransaction.CreatedAt = now;
                    copiedTransaction.TransactionFrequency = TransactionFrequency.OneTime;
                    copiedTransaction.Categories = t.Categories;

                    return copiedTransaction;
                })
                .ToList();
            await _dbContext.Transactions.AddRangeAsync(copiedTransactions);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<(bool, string)> ImportTransactions(IFormFile file, User user)
        {
            using var reader = new StreamReader(file.OpenReadStream());
            var jsonString = await reader.ReadToEndAsync();
            var transactions = JsonSerializer.Deserialize<List<TransactionViewDto>>(jsonString);
            if (transactions == null)
            {
                return (false, "No transactions found");
            }
            var res = new List<Transaction>();

            foreach (var transaction in transactions)
            {
                var entity = transaction.Adapt<Transaction>();
                entity.Vendor = null;

                entity.VendorId = await FetchVendorId(transaction, entity);

                var categoryIds = await FetchCategoryIds(transaction);

                var categories =
                    categoryIds != null
                        ? await _dbContext
                            .Categories.Where(c => categoryIds.Contains(c.Id))
                            .ToListAsync()
                        : new List<Category>();

                entity.Categories = categories;

                entity.UserId = user.Id;
                entity.CreatedAt = DateTime.Now;
                res.Add(entity);
            }

            _dbContext.Transactions.AddRange(res);
            await _dbContext.SaveChangesAsync();

            return (true, "Transactions imported successfully.");
        }

        private async Task<int> FetchVendorId(TransactionViewDto transaction, Transaction entity)
        {
            var vendorDictionary = (await _dbContext.Vendors.ToListAsync()).ToDictionary(
                c => c.Name,
                c => c.Id
            );

            if (vendorDictionary.TryGetValue(transaction.Vendor!.Name, out int vendorId))
            {
                return vendorId;
            }
            else
            {
                throw new InvalidDataException(
                    "Vendor with this name does not exist in application"
                );
            }
        }

        private async Task<List<int>> FetchCategoryIds(TransactionViewDto transaction)
        {
            var categoryDictionary = (await _dbContext.Categories.ToListAsync()).ToDictionary(
                c => c.Name,
                c => c.Id
            );
            var categoryIds = new List<int>();
            foreach (var categoryName in transaction.Categories.Select(c => c.Name))
            {
                if (categoryDictionary.TryGetValue(categoryName, out int categoryId))
                {
                    categoryIds.Add(categoryId);
                }
                else
                {
                    throw new InvalidDataException(
                        "Category with this name does not exist in application"
                    );
                }
            }
            return categoryIds;
        }

        private void ApplyFilters(ref IQueryable<Transaction> query, FilterOptionsDto options)
        {
            query = options.UserId != null ? query.Where(t => t.UserId == options.UserId) : query;
            query =
                options.LowerLimit != null
                    ? query.Where(t => t.Amount >= options.LowerLimit)
                    : query;
            query =
                options.UpperLimit != null
                    ? query.Where(t => t.Amount <= options.UpperLimit)
                    : query;
            query = !options.Expense
                ? query.Where(t => t.TransactionType != TransactionType.Expense)
                : query;
            query = !options.Income
                ? query.Where(t => t.TransactionType != TransactionType.Income)
                : query;
            query = options.From != null ? query.Where(t => t.CreatedAt >= options.From) : query;
            query =
                options.To != null
                    ? query.Where(t => t.CreatedAt < ((DateTime)options.To).Date.AddDays(1))
                    : query;
            query =
                options.VendorIds != null && options.VendorIds.Any()
                    ? query.Where(t => options.VendorIds!.Contains(t.VendorId))
                    : query;
            query =
                options.CategoryIds != null && options.CategoryIds.Any()
                    ? query.Where(t => t.Categories.Any(c => options.CategoryIds!.Contains(c.Id)))
                    : query;
        }
    }
}
