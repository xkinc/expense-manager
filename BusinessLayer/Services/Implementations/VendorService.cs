﻿using BusinessLayer.Models;
using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Data;
using DataAccessLayer.Entities;

namespace BusinessLayer.Services.Implementations
{
    public class VendorService : BaseService<Vendor, VendorDto>, IVendorService
    {
        public VendorService(ManagerDbContext dbContext)
            : base(dbContext) { }
    }
}
