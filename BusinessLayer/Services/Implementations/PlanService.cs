﻿using BusinessLayer.Models;
using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Data;
using DataAccessLayer.Entities;

namespace BusinessLayer.Services.Implementations
{
    public class PlanService : BaseService<Plan, PlanDto>, IPlanService
    {
        public PlanService(ManagerDbContext dbContext)
            : base(dbContext) { }
    }
}
