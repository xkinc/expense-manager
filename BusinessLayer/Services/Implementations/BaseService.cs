﻿using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Data;
using DataAccessLayer.Entities;
using Mapster;
using Microsoft.EntityFrameworkCore;

namespace BusinessLayer.Services.Implementations
{
    //IBaseService could be deleted here, but it shows that this class must implement IBaseService
    public abstract class BaseService<TEntity, TDto> : IBaseService<TDto>
        where TEntity : BaseEntity
        where TDto : class
    {
        protected readonly ManagerDbContext _dbContext;

        protected BaseService(ManagerDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<TDto?> CreateAsync(TDto dto)
        {
            var entity = dto.Adapt<TEntity>();
            if (entity.CreatedAt == default)
            {
                entity.CreatedAt = DateTime.Now;
            }
            var res = _dbContext.Set<TEntity>().Add(entity);

            await _dbContext.SaveChangesAsync();

            return res.Adapt<TDto?>();
        }

        public virtual async Task<bool> DeleteAsync(int id)
        {
            var entity = await GetEntityAsync(id);
            if (entity == null)
            {
                return false;
            }
            var res = _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync();
            return res != null;
        }

        public virtual async Task<List<TDto>> GetAllAsync()
        {
            var entities = await _dbContext.Set<TEntity>().ToListAsync();
            return entities.Adapt<List<TDto>>();
        }

        public virtual async Task<TDto?> GetByIdAsync(int id)
        {
            var entity = await GetEntityAsync(id);
            if (entity == null)
            {
                return null;
            }
            return entity.Adapt<TDto>();
        }

        public virtual async Task<TDto?> UpdateAsync(int id, TDto dto)
        {
            var entity = await GetEntityAsync(id);
            if (entity == null)
            {
                return null;
            }

            var updatedEntity = dto.Adapt(entity);
            updatedEntity.Id = id;

            var res = _dbContext.Set<TEntity>().Update(updatedEntity);
            await _dbContext.SaveChangesAsync();
            return res.Adapt<TDto?>();
        }

        protected async Task<TEntity?> GetEntityAsync(int id)
        {
            var query = _dbContext.Set<TEntity>().Where(e => e.Id == id);
            if (typeof(TEntity) == typeof(Transaction))
            {
                query = query.Include("Categories").Include("Vendor");
            }
            var entity = await query.FirstOrDefaultAsync();
            return entity;
        }
    }
}
