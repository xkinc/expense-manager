﻿using BusinessLayer.Models;
using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Data;
using DataAccessLayer.Entities;

namespace BusinessLayer.Services.Implementations
{
    public class CategoryService : BaseService<Category, CategoryDto>, ICategoryService
    {
        public CategoryService(ManagerDbContext dbContext)
            : base(dbContext) { }
    }
}
