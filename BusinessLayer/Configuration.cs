﻿using BusinessLayer.Services.Implementations;
using BusinessLayer.Services.Interfaces;
using DataAccessLayer.Data;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BusinessLayer
{
    public static class Configuration
    {
        public static IServiceCollection AddServices(
            this IServiceCollection services,
            string connection
        )
        {
            services.AddMyContext(connection);

            services
                .AddIdentity<User, IdentityRole<int>>(options =>
                {
                    options.User.RequireUniqueEmail = true;
                })
                .AddEntityFrameworkStores<ManagerDbContext>()
                .AddDefaultUI()
                .AddDefaultTokenProviders();

            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IVendorService, VendorService>();
            services.AddScoped<IPlanService, PlanService>();
            return services;
        }

        private static IServiceCollection AddMyContext(
            this IServiceCollection services,
            string connection
        )
        {
            services.AddDbContext<ManagerDbContext>(options => options.UseSqlite(connection));
            return services;
        }
    }
}
